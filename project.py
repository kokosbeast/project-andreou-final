import streamlit as st
import boto3
import json
from datetime import datetime

s3 = boto3.resource('s3')

st.title("ΜΗΥΠ 521 (463) – Προχωρημένα Θέματα Τεχνολογίας Λογισμικού")
st.header("Web App για έξυπνη διαχείριση ιατρικών φακέλων")

obj1 = s3.Object('pjht12source', 'patients.json')
file1 = obj1.get()['Body'].read().decode('utf-8')
json1 = json.loads(file1)


obj2 = s3.Object('pjht12source', 'diagnoses.json')
file2 = obj2.get()['Body'].read().decode('utf-8')
json2 = json.loads(file2)


obj3 = s3.Object('pjht12source', 'medicines.json')
file3 = obj3.get()['Body'].read().decode('utf-8')
json3 = json.loads(file3)


v1=0
v2=0
v3=0
arrayP = []
arrayD = []
arrayM = []

for k in json1['Patients']:
    arrayP.append(json1['Patients'][v1]['Name'] + " " + json1['Patients'][v1]['Surname'])
    v1+=1

for u in json2['Diagnoses']:
    arrayD.append(json2['Diagnoses'][v2]['Name'])
    v2+=1

for y in json3['Medicines']:
    arrayM.append(json3['Medicines'][v3]['Brand'])
    v3+=1


patient = st.selectbox("Επιλέξτε ασθενή:",arrayP)
st.markdown("<p style=' color: blue;'>Επιλέξατε: "+patient+"</p>", unsafe_allow_html=True)
pCode = json1['Patients'][arrayP.index(patient)]['Code']

diagnosis = st.selectbox("Επιλέξτε διάγνωση:",arrayD)
st.markdown("<p style=' color: blue;'>Επιλέξατε: "+diagnosis+"</p>", unsafe_allow_html=True)
dCode = json2['Diagnoses'][arrayD.index(diagnosis)]['Code']
dName= json2['Diagnoses'][arrayD.index(diagnosis)]['Name']

medicine = st.selectbox("Επιλέξτε φάρμακο:",arrayM)
st.markdown("<p style=' color: blue;'>Επιλέξατε: "+medicine+"</p>", unsafe_allow_html=True)
mCode = json3['Medicines'][arrayM.index(medicine)]['Code']


if st.button('Καταχώρηση'):
    flag=0
    today = datetime.today().strftime('%d/%m/%Y')
    for h in json3['Medicines'][arrayM.index(medicine)]['Contraindications']:
        if(dCode==h):
            flag+=1

    if(flag>0):
        st.error('Αυτό το φάρμακο δεν μπορεί να δωθεί στον ασθενή λόγω του ότι δεν είναι συμβατό: '+dName)
    elif(flag<1):
            obj4 = s3.Object('pjht12destination', pCode+'.json')
            file4 = obj4.get()['Body'].read().decode('utf-8')
            json4 = json.loads(file4)
            data = {"date": today,
            "diagnosis_code": dCode,
            "medicine_code": mCode
            }
            placeholder = json4['Diagnoses']
            placeholder.append(data)
            obj5 = s3.Object('pjht12destination', pCode+'.json')
            obj5.put(Body=(bytes(json.dumps(json4).encode('UTF-8'))))
            st.success('Η διάγνωση καταχωρήθηκε επιτυχώς!')
else:
    st.markdown("<p style=' color: blue; font-weight: bold;'>Πατήστε το πιο πάνω κουμπί για καταχώρηση της διάγνωσης!</p>", unsafe_allow_html=True)



st.markdown("Ομάδα φοιτητών: Αναστάσης Αναστάση, Ανδρόνικος Ευαγγέλου")